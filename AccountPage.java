package be.ordina.pages;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccountPage {
    private final WebDriver driver;
    private WebDriverWait wait ;
    private Actions actions;

    public AccountPage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
        this.actions = new Actions(driver);

    }

    private By accountDeletionMarker = By.xpath("/html/body/div/div[4]/div/div/div[2]/div/div[11]/div/a");
//Somehow wrong element: clicking it with regular xpath, full xpath, or by linktext, all yields the same result: redirect to login page.

    public void deleteAccount(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(accountDeletionMarker));
        WebElement accountDeletion = driver.findElement(accountDeletionMarker);
        wait.until(ExpectedConditions.elementToBeClickable(accountDeletion));
        //Trying to cheat because regular clicking on the element results in a redirect. It still redirects with this.
        actions.moveToElement(accountDeletion).click().build().perform();
    }

}
