package be.ordina.pages;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeletionPage {
    private final WebDriver driver;
    private WebDriverWait wait ;

    public DeletionPage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);

    }

    private By passwordMarker = By.id("user[password]");

    public void deleteAccount(String password){
        wait.until(ExpectedConditions.visibilityOfElementLocated(passwordMarker));
        WebElement passwordBox = driver.findElement(passwordMarker);
        passwordBox.sendKeys(password);
        passwordBox.submit();
    }


}
