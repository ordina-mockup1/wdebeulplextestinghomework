package be.ordina.pages;
import org.openqa.selenium.*;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class MovieDetailPage {
    private final WebDriver driver;
    private WebDriverWait wait;


    public MovieDetailPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 20);
    }

    private By watchListBtnMarker = By.xpath("/html/body/div[1]/div[4]/div/div/div[3]/div[2]/div/div[1]/div[2]/div[4]/button[2]");
    //the id of the button seems to increment every time it's pushed, so normal Xpath doesn't work reliably here.
    private By moviesShowsBtnMarker = By.xpath("//*[@id=\"content\"]/div/div/div[2]/div/div/div/div[2]/a");
    private By movieTitleMarker = By.xpath("//*[@id=\"content\"]/div/div/div[3]/div[2]/div/div[1]/div[2]/div[1]/div/span");


    public void ClickWatchListBtn(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(watchListBtnMarker));
        WebElement watchListBtn = driver.findElement(watchListBtnMarker);
        watchListBtn.click();
    }

    public void ClickmoviesShowsBtn(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(moviesShowsBtnMarker));
        WebElement moviesShowsBtn = driver.findElement(moviesShowsBtnMarker);
        moviesShowsBtn.click();
    }

    public String readMovieTitle(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(movieTitleMarker));
        WebElement movieTitle = driver.findElement(movieTitleMarker);
        String movieTitleText = movieTitle.getText();
        return  movieTitleText;
    }

}