package be.ordina;

import static org.junit.Assert.assertTrue;


import be.ordina.pages.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


/**
 * Unit test for simple App.
 */




public class AppTest
{
    private WebDriver driver;
    /**
     * Rigorous Test :-)
     */


    @Before
    public void setUp(){

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-fullscreen");
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\WiDB\\Documents\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver(options);
        driver.get("https://www.plex.tv/");

    }
    //Create and sign in on a new account, confirm that it leads to the homepage, delete this new account?

    @Test
    public void homePageCheckNewAccount() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        SignUpModal signUpModal = new SignUpModal(driver);
        HomePage homePage = new HomePage(driver);
        AccountPage accountPage = new AccountPage(driver);
        DeletionPage deletionPage = new DeletionPage(driver);
        loginPage.clickSignup();
// testing e-mail: grimebeer@mailinator.com
// password : P455w0rd!
        //Scope of automating login generation? As it's written here, it doesn't test for random usernames, which can be useful if eventually
        // we have to test whether offensive terms in usernames are to be banned.

        signUpModal.login("grimebeer011@mailinator.com", "P455w0rd!");
        String message = homePage.checkCurrentURL();
        System.out.println(message);

        assertTrue(message.contains("app.plex.tv/desktop"));
//optional: delete the account we just made. Separate test? The main purpose would be to be able to reuse the same address,
// but the site blocks you from using the same e-mail for a month after deleting, so not much of a point to it for that.
 //The site also redirects to the login-page when clicking the link, wrong element? Tweaking it yields no results. What's the expected behaviour?
    /*   homePage.goToAccountPage();
        Thread.sleep(5000);
        accountPage.deleteAccount();
        Thread.sleep(5000);
        deletionPage.deleteAccount("P455w0rd!");*/

    }
//Testing the workflow to get unto homepage with an established account: if this one fails, the others will, too, and it's easy to fix this one.
@Test
    public void homePageSignIntoOldAccount(){
    LoginPage loginPage = new LoginPage(driver);
    SignUpModal signUpModal = new SignUpModal(driver);
    HomePage homePage = new HomePage(driver);
    loginPage.clickSignin();

    signUpModal.login("grimbeer12@mailinator.com", "P455w0rd!");
    loginPage.hitLaunch();

    String message = homePage.checkCurrentURL();
    System.out.println(message);

    assertTrue(message.contains("app.plex.tv/desktop"));

}

//sign into established account, navigate to homepage, find a film, click on play button, assert that it is streaming

    @Test
    public void checkIfStreaming() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        SignUpModal signUpModal = new SignUpModal(driver);
        HomePage homePage = new HomePage(driver);
        loginPage.clickSignin();

        signUpModal.login("grimbeer12@mailinator.com", "P455w0rd!");
        loginPage.hitLaunch();
        homePage.clickOnPlayButton();
        //sloppy, but couldn't find an element to wait for while the movie is loading
        Thread.sleep(5000);
        //How do we know if the player specifically is streaming? If the play button has become a pause button.
        assertTrue(homePage.isThereAPauseButton());

    }


    @Test
    public void searchBoxTesting(){
        LoginPage loginPage = new LoginPage(driver);
        SignUpModal signUpModal = new SignUpModal(driver);
        HomePage homePage = new HomePage(driver);
        MovieDetailPage movieDetailPage = new MovieDetailPage(driver);

        loginPage.clickSignin();
        //can be expanded to have it ask the user for input, then check the length and if it's more than 3 characters to proceed.
        //out of scope, though?
        String testSearch = "poly";

        signUpModal.login("grimbeer12@mailinator.com", "P455w0rd!");
        loginPage.hitLaunch();
        System.out.println("First search result of " + testSearch + " is " +homePage.GetFirstResultFromSearch(testSearch));
        String clickedMovie = homePage.GetFirstResultFromSearch(testSearch);
        String selectedMovie = movieDetailPage.readMovieTitle();
        System.out.println("Resulting movie is: " + selectedMovie);
        //making sure it's all converted to lowercase, so as not to pollute the results:
        String movieCheck = selectedMovie.toLowerCase();
        String searchCheck = testSearch.toLowerCase();
        //asserting that the suggestion is relevant:
        assertTrue(movieCheck.contains(searchCheck));
        //asserting that clicking the movie title leads to the right detail page:
        assertTrue(clickedMovie.compareTo(selectedMovie) == 0);


}

@Test
public void watchListTesting() {
    LoginPage loginPage = new LoginPage(driver);
    SignUpModal signUpModal = new SignUpModal(driver);
    HomePage homePage = new HomePage(driver);
    MovieDetailPage movieDetailPage = new MovieDetailPage(driver);
    WatchListPage watchListPage = new WatchListPage(driver);
    loginPage.clickSignin();

    signUpModal.login("grimbeer12@mailinator.com", "P455w0rd!");
    loginPage.hitLaunch();
    homePage.clickOnIcon();

    String testedFilm = movieDetailPage.readMovieTitle();
    //using System.out.println to troubleshoot and tweak where necessary
    System.out.println("Chosen film is: " + testedFilm);
    movieDetailPage.ClickWatchListBtn();
    movieDetailPage.ClickmoviesShowsBtn();
    homePage.clickWatchListTabBtn();
    String addedFilm = watchListPage.readMovieTitle();
    System.out.println("The added film is " + addedFilm);
    System.out.println(testedFilm.compareTo(addedFilm));
    //assert that it is the correct film that was added.
    assertTrue(testedFilm.compareTo(addedFilm) == 0);

    //go back to the movie page and remove the film from watchlist
    watchListPage.goToMoviePage();
    movieDetailPage.ClickWatchListBtn();
    //Go back to watchlist: if it's empty, then we've also proven the button changed
    movieDetailPage.ClickmoviesShowsBtn();
    homePage.clickWatchListTabBtn();
    System.out.println(watchListPage.isWatchListEmpty());
    assertTrue(watchListPage.isWatchListEmpty());


}


    @After
    public void tearDown(){
        driver.close();
    }




}
