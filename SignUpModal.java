package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignUpModal {

    private final WebDriver driver;
    private WebDriverWait wait ;

    public SignUpModal(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 20);
    }

    private By emailField = By.id("email");
    private By passwordField = By.id("password");

    private By frame = By.id("fedauth-iFrame");





    public void login (String username, String password) {
       // WebElement loginFrame = driver.findElement(By.id("fedauth-iframe"));

        wait.until(ExpectedConditions.visibilityOfElementLocated(frame));
        driver.switchTo().frame("fedauth-iFrame");

        wait.until(ExpectedConditions.visibilityOfElementLocated(emailField));
        WebElement element1= driver.findElement(emailField);
        element1.sendKeys(username);
        WebElement element = driver.findElement(passwordField);
        element.sendKeys(password);
        element.submit();
    }


}
