package be.ordina.pages;
import org.openqa.selenium.*;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
public class WatchListPage {
    private final WebDriver driver;
    private WebDriverWait wait;


    public WatchListPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    private By movieTitleMarker = By.xpath("/html/body/div/div[4]/div/div/div[3]/div[3]/div[1]/div/div/div[2]/a");

    private By emptyPageMarker = By.className("EmptyPage-emptyPage-2AM9uQ");

    //InnerEmptyPage-innerEmptyPage-1MHKZS EmptyPage-emptyPage-2AM9uQ Scroller-scroller-3GqQcZ Scroller-none-3GfKRp
    public String readMovieTitle(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(movieTitleMarker));
        WebElement movieTitle = driver.findElement(movieTitleMarker);
        String movieTitleText = movieTitle.getText();
        return  movieTitleText;
    }

    public void goToMoviePage(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(movieTitleMarker));
        WebElement movieTitle = driver.findElement(movieTitleMarker);
        //using the easier method of clicking on the title rather than fiddling around with coordinates and actions. We only need one element to navigate here
        movieTitle.click();
    }

    public boolean isWatchListEmpty(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(emptyPageMarker));
        WebElement emptyPage = driver.findElement(emptyPageMarker);
        if(emptyPage.isDisplayed()){
            return true;
        }
        else{
            return false;
        }

    }
}
