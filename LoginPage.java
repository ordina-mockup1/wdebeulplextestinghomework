package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {

    //setting up
    private final WebDriver driver;
    private WebDriverWait wait ;

    public LoginPage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    private By signupBtn = By.className("signup");
    private By loginBtn = By.className("signin");
    private By launchBtnMarker = By.className("launch");


    public void clickSignup(){
        wait.until(ExpectedConditions.elementToBeClickable(signupBtn));
        driver.findElement(signupBtn).click();

    }

    public void clickSignin(){
        wait.until(ExpectedConditions.elementToBeClickable(loginBtn));
        driver.findElement(loginBtn).click();

    }

    public void hitLaunch(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(launchBtnMarker));
        WebElement launchBtn = driver.findElement(launchBtnMarker);
        launchBtn.click();

    }

}
