package be.ordina.pages;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
    private final WebDriver driver;
    private WebDriverWait wait ;
    private Actions actions;

    public HomePage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 20);
        this.actions = new Actions(driver);

    }



    private By homePageMarker = By.className("PageHeaderBreadcrumbButton-link-30Jgzu");

    private By accountMarker = By.className("DisclosureArrow-medium-20IdwE");
    private By accountLinkMarker = By.partialLinkText("count");

    private By dropDownListMarker = By.id("id-13");

    private By movieIconMarker = By.xpath("/html/body/div/div[4]/div/div/div[3]/div[2]/div[1]/div[1]/div[2]/div/div/div[4]/div/a");


    private By playButtonMarker = By.xpath("//*[@id='content']/div/div/div[3]/div[2]/div[1]/div[1]/div[2]/div/div/div[4]/div/button[1]");

    //relevant when playing a film
    private By moviePlayerMarker = By.id("plex");
    private By pauseButtonMarker = By.xpath("//*[@id='plex']/div[6]/div/div[3]/div/div/div[2]/div[2]/button[3]");

    //unused for now, may be useful later.
  //  private By backgroundMarker = By.className("background-container");
    // private By movieMenuMarker = By.className("BottomBar-bottomBar-gbDBaF");

    private By watchListTabButtonMarker = By.xpath("//*[@id=\"content\"]/div/div/div[3]/div[1]/div[2]/div[1]/a[2]");

    //Testing the searchbox
    private By searboxMarker = By.className("QuickSearchInput-searchInput-3m6naA");
    //private By searchResultsMarker = By.className("QuickSearchResults-container-3IlHhN");
    private By firstResultMarker = By.xpath("//*[@id=\"plex\"]/div[2]/div/div[1]/div/div[2]/div[2]/div[1]/div[1]/div/div[2]/div[1]/span");
//QuickSearchResults-container-3IlHhN  Scroller-scroller-3GqQcZ Scroller-vertical-VScFLT
    public String checkCurrentURL(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(accountMarker));
        String urlresult = driver.getCurrentUrl();
        return urlresult;
    }

    public void goToAccountPage(){

        wait.until(ExpectedConditions.visibilityOfElementLocated(accountMarker));
        WebElement ddl = driver.findElement(accountMarker);
        ddl.click();


        wait.until(ExpectedConditions.visibilityOfElementLocated(dropDownListMarker));
        WebElement accountBtn = driver.findElement(accountLinkMarker);
        accountBtn.click();

        }
    public void clickOnIcon(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(movieIconMarker));
        WebElement movieIcon = driver.findElement(movieIconMarker);
        wait.until(ExpectedConditions.elementToBeClickable(movieIcon));

            //By default, the method 'Click' will click on the center of an object. Clicking in the center of the icon is functionally the same as clicking the play button
            // we want to click on the icon, outside of the play button.
        Point Coordinates = movieIcon.getLocation();
//playbutton appears on hover over, so we add an Actions action to this class.
        actions.moveToElement(movieIcon).perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(playButtonMarker));
        WebElement playButton = driver.findElement(playButtonMarker);
        System.out.println("X-coord: "+ Coordinates.x + " Y-coord: " + Coordinates.y);

        Dimension buttonSize = playButton.getSize();

        System.out.println("The play button is " + buttonSize.height + "high" + " and "+ buttonSize.width + " wide");

//There's a 10 pixel difference between the linking object and the svg surrounding it. Fiddling a little, in principle it can be cleaned up by measuring play button, but the overreaching svg is larger, and cannot be clicked
       /* Coordinates.x +=  buttonSize.width;
        Coordinates.y +=  buttonSize.height;
        Even if I make both these -7 smaller, so it also clicks with an offset of 25, the actions.moveToElement doesn't yield the same result.
        */

//These coordinates are right on the edge of the play button
        actions.moveToElement(movieIcon,25, 25).click().build().perform();
        //moveToElement uses the top left coordinates, not the center point? It acts as though it uses the center, point, though.
        System.out.println("X-coord: "+ Coordinates.x + " Y-coord: " + Coordinates.y);

        }

    public void clickOnPlayButton(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(movieIconMarker));
        WebElement movieIcon = driver.findElement(movieIconMarker);
        actions.moveToElement(movieIcon).perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(playButtonMarker));
        WebElement playButton = driver.findElement(playButtonMarker);
        playButton.click();

    }


    public boolean isThereAPauseButton(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(moviePlayerMarker));
        WebElement moviePlayer = driver.findElement(moviePlayerMarker);
        //Again, this is setting up for possible future improvements, using relative proportions to move the cursor,
        // but it looks like Actions don't work that well with relative values.

       // Dimension screenSize = moviePlayer.getSize();
       // actions.moveToElement(moviePlayer, -50, -300).perform();

        //System.out.println("Screen has a size of" + screenSize.width + "by" +screenSize.height);
        //int lowerMargin = screenSize.height * 4/5 ;

        //wait.until(ExpectedConditions.invisibilityOfElementLocated(pauseButtonMarker));

        actions.moveToElement(moviePlayer, 100, 50).build().perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(pauseButtonMarker));
        WebElement pauseButton = driver.findElement(pauseButtonMarker);

        String status = pauseButton.getAttribute("title");
        System.out.println(status);
        if (status.contains("Pause")) {
            return true;
        }
        else{
          return false;
        }
    }

    public void clickWatchListTabBtn(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(watchListTabButtonMarker));
        WebElement watchListTabBtn = driver.findElement(watchListTabButtonMarker);
        watchListTabBtn.click();

    }

    public String GetFirstResultFromSearch(String input){
        wait.until(ExpectedConditions.visibilityOfElementLocated(searboxMarker));
        WebElement searchBox= driver.findElement(searboxMarker);
        searchBox.sendKeys(input);

        wait.until(ExpectedConditions.visibilityOfElementLocated(firstResultMarker));

        WebElement firstResult = driver.findElement(firstResultMarker);
        String firstResultText = firstResult.getText();
        actions.moveToElement(firstResult).click().build().perform();
        return firstResultText;


    }

}
